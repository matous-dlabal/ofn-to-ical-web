# Generování událostí iCal z dat dle OFN - web

Webová stránka prezentující knihovnu
pro generování událostí ve formátu iCalendar z dat dle OFN.
Součást bakalářské práce
*Generování kalendářových událostí z dat dle Otevřených formálních norem*
vytvořené v rámci studia
na [Fakultě Informačních Technologií ČVUT](https://fit.cvut.cz).

Online verze této stránky je dostupná
[zde](https://matous-dlabal.gitlab.io/ofn-to-ical-web/).

# Dokumentace

Programátorská dokumentace se nachází v textu práce v kapitole
[7.1.3](https://matous-dlabal.gitlab.io/ofn-to-ical-web/bakalarska-prace-matous-dlabal.pdf#5a).

Dokumentaci JavaScriptové části stránky naleznete
[zde](https://matous-dlabal.gitlab.io/ofn-to-ical-web/doc/index.html).

