/**
 * Tlačítko 'Generovat'.
 * Po stisku volá funkci {@link generate}.
 */
const generateBtn = document.getElementById('generateBtn');
generateBtn.onclick = generate;

/**
 * Element input s typem file, kterým se vybítají lokální soubory.
 * Po výběru se zavolá funkce {@link loadFromFile}.
 */
const fileSelector = document.getElementById('fileSelector');
fileSelector.onchange = loadFromFile;

/**
 * Tlačítko 'Nahrát lokální soubor'.
 * Stisk tlačítka vyvolá stisk selectoru {@link fileSelector}.
 */
const loadFromFileBtn = document.getElementById('loadFromFileBtn');
// clicking the button will open the file selector
loadFromFileBtn.onclick = () => fileSelector.click();

/**
 * Tlačítko 'Nahrát soubor z internetu'.
 * Stisk zavolá funkci {@link loadFromUrl}.
 */
const loadFromUrlBtn = document.getElementById('loadFromUrlBtn');
loadFromUrlBtn.onclick = loadFromUrl;

/**
 * Element select pro výběr ukázkových souborů.
 * Po výběru se zavolá funkce {@link setUrlFromSelectedExample}.
 */
const exampleSelect = document.getElementById('exampleSelect');
exampleSelect.onchange = setUrlFromSelectedExample;

/**
 * HTML element, který slouží pro upozorňování uživatele
 * (např. o problémech při generování).
 * @namespace
 */
const mainAlert = document.getElementById('main-alert');
/**
 * Schová element {@link mainAlert}.
 * @return {undefined}
 */
mainAlert.hide = () => mainAlert.classList.add('d-none');
/**
 * Zobratí element {@link mainAlert}.
 * @param {string} text - Text, který se má zobrazit.
 * @return {undefined}
 */
mainAlert.show = (text) => {
  mainAlert.innerHTML = text;
  mainAlert.classList.remove('d-none');
};

/**
 * HTML element, který slouží pro indikaci nahraných dat.
 * @namespace
 */
const loadedAlert = document.getElementById('loadedAlert');
/**
 * Schová element {@link loadedAlert}.
 * @return {undefined}
 */
loadedAlert.hide = () => loadedAlert.classList.add('invisible');
/**
 * Zobratí element {@link loadedAlert}.
 * @param {string} filename - Název souboru, který byl nahrán.
 * @return {undefined}
 */
loadedAlert.show = (filename) => {
  loadedAlert.innerHTML = `Nahrán soubor ${filename}`;
  loadedAlert.classList.remove('invisible');
};

/**
 * Obsahuje uživatelem nahraná data.
 * Pokud žádná data ještě nebyly nahrány, má proměnná hodnotu undefined.
 * @type string
 */
let loadedData;


/**
 * Funkce volaná stiskem tlačítka 'Generovat'.
 * Načte potřebné hodnoty z formuláře a zavolá metodu
 * {@link generateWithService}, nebo {@link generateWithLib}.
 * Schová dříve zobrazená varování (element {@link mainAlert}).
 * <br>
 * Pokud nejsou načtená žádná data, informuje o tom uživatele pomocí
 * html elementu {@link mainAlert}.
 *
 * @return {undefined}
 */
function generate() {
  mainAlert.hide();

  if (loadedData === undefined) {
    mainAlert.show('Nebyla nahrána žádná data.');
    return;
  }

  const langCode = document.getElementById('languageSelect').value;
  const asOneCalendar = document.getElementById('asOneCalendar').checked;

  const useHttpService = document.getElementById('useHTTPService').checked;

  if (useHttpService) {
    generateWithService(loadedData, langCode, asOneCalendar);
  } else {
    generateWithLib(loadedData, langCode, asOneCalendar);
  }
}

/**
 * Funkce volaná při změně výběru lokálního souboru
 * elementem {@link fileSelector}.
 * Do proměnné {@link loadedData} načte (textově) obsah souboru,
 * schová varování {@link mainAlert} a indikuje načtení dat pomocí
 * elementu {@link loadedAlert}.
 *
 * @return {undefined}
 */
function loadFromFile() {
  const file = fileSelector.files[0];
  const fr = new FileReader();
  fr.onload = () => {
    loadedData = fr.result;
  };
  fr.readAsText(file);

  mainAlert.hide();
  loadedAlert.show(file.name);
}

/**
 * Funkce volaná při změně výběru ukázkového souboru.
 * Do prvku inputUrl nastaví URL ukázového souboru.
 * @return {undefined}
 */
function setUrlFromSelectedExample() {
  let baseUrl = document.location.href;
  baseUrl = baseUrl.slice(0, baseUrl.lastIndexOf('/'));
  let exampleUrl = '';
  if (exampleSelect.value !== '') {
    exampleUrl = `${baseUrl}/examples/${exampleSelect.value}`;
  }
  document.getElementById('inputUrl').value = exampleUrl;
}

/**
 * Funkce volaná stiskem tlačítka 'Nahrát soubor z internetu'.
 * Načte hodnotu z pole inputUrl, pokud nezačíná na http,
 * přidá před URL http:// (jinak je problém s načítáním).
 * <br>
 * Pokud se data podařilo načíst,
 * nastaví proměnnou {@link loadedData}, schová varování {@link mainAlert}
 * a indikuje načtení dat pomocé elementu {@link loadedAlert}.
 * <br>
 * Pokud se data z internetu načíst nepodařilo,
 * zobrazí se varování (pomocí elementu {@link mainAlert}).
 *
 * @return {undefined}
 */
function loadFromUrl() {
  let url = document.getElementById('inputUrl').value;
  if (!url.startsWith('http')) {
    url = 'http://' + url;
  }

  fetch(url).then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    return response.text();
  }).then((data) => {
    loadedData = data;

    mainAlert.hide();
    loadedAlert.show(url.substring(url.lastIndexOf('/')+1));
  }).catch((error) => {
    mainAlert.show('Data ze zadané URL nejdou nahrát.');
  });
}

/**
 * Pomocí HTTP služby vygeneruje kalendářové události a uloží je.
 * <br>
 * Pokud při generování nastane chyba, zobrazí se uživateli (pomocí
 * elementu {@link mainAlert}) upozornění.
 *
 * @param {string} loadedData - Data dle OFN ve formátu JSON-LD.
 * @param {string} langCode - Kód jazyka dle ISO 639-1.
 * @param {boolean} asOneCalendar - True, pokud chceme všechny události
 *  součit do jednoho kalendáře.<br>
 *  False, pokud chceme zachovat rozčlenění danné strukturou
 *  vstupních dat -- seznam dat dle OFN.
 *
 * @return {undefined}
 */
function generateWithService(loadedData, langCode, asOneCalendar) {
  const server = 'https://ofn-to-ical-service.herokuapp.com';
  const endpoint = `${server}/api/kalendář/${asOneCalendar ? 'jeden' : 'více'}`;

  fetch(endpoint, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/ld+json',
      'Accept-Language': langCode,
    },
    body: loadedData,
  })
      .then((res) => {
        if (res.status === 200) {
          return asOneCalendar ? res.text() : res.json();
        }
        if (res.status === 400) {
          res.json().then((e) => {
            mainAlert.show(`Při generování nastala chyba. (${e.error})`);
          });
          return;
        }
        mainAlert.show('Při použití HTTP služby nastala chyba');
      })
      .then((data) => {
        if (data) {
          if (asOneCalendar) {
            saveICal('ofn-to-ical.ics', data);
          } else {
            createAndSaveZip('ofn-to-ical.zip', data);
          }
        }
      })
      .catch((error) => {
        console.error('Error:', error);
        mainAlert.show('Při použití HTTP služby nastala chyba');
      });
}

/**
 * Pomocí knihovny vygeneruje kalendářové události a uloží je.
 * <br>
 * Pokud při generování nastane chyba, zobrazí se uživateli (pomocí
 * elementu {@link mainAlert}) upozornění.
 *
 * @param {string} loadedData - Data dle OFN ve formátu JSON-LD.
 * @param {string} langCode - Kód jazyka dle ISO 639-1.
 * @param {boolean} asOneCalendar - True, pokud chceme všechny události
 *  součit do jednoho kalendáře.<br>
 *  False, pokud chceme zachovat rozčlenění danné strukturou
 *  vstupních dat -- seznam dat dle OFN.
 *
 * @return {undefined}
 */
function generateWithLib(loadedData, langCode, asOneCalendar) {
  try {
    const icals = OFNCAL.generate(loadedData, langCode, asOneCalendar);

    if (asOneCalendar) {
      saveICal('ofn-to-ical.ics', icals[0]);
    } else {
      createAndSaveZip('ofn-to-ical.zip', icals);
    }
  } catch (e) {
    mainAlert.show('Při generování nastala chyba. (' + e.message + ')');
  }
}

/**
 * Vytvoří Blob obsahující zadaná data a typ icalendar s kódováním utf-8.
 * @param {string} data - Data ve formátu iCalendar.
 * @return {Blob}
 */
function createICalBlob(data) {
  return new Blob([data], {type: 'text/calendar;charset=utf-8;'});
}

/**
 * Uloží zadaný Blob.
 * @param {string} filename - Název ukládaného souboru.
 * @param {Blob} blob - Blob obsahující data k uložení.
 * @return {undefined}
 */
function saveBlob(filename, blob) {
  const elem = document.createElement('a');
  elem.href = URL.createObjectURL(blob);
  elem.download = filename;
  elem.click();
  URL.revokeObjectURL(blob);
}

/**
 * Uloží textová iCalendar data.
 * @param {string} filename - Název ukládaného souboru.
 * @param {string} data - Data ve formátu iCalendar.
 * @return {undefined}
 */
function saveICal(filename, data) {
  saveBlob(filename, createICalBlob(data));
}

/**
 * Uloží pole iCaledar data jako zip jednotlivách souborů.
 * @param {string} filename - Název ukládaného souboru.
 * @param {Array<string>} icalArray - Pole dat ve formátu iCalendar.
 * @return {undefined}
 */
function createAndSaveZip(filename, icalArray) {
  const zip = new JSZip();

  icalArray.forEach((ical, index, array) => {
    zip.file(`${index}.ics`, ical);
  });

  zip.generateAsync({type: 'blob'})
      .then((content) => saveBlob(filename, content));
}
