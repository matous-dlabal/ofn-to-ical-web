module.exports = function(grunt) {
  grunt.initConfig({
    jsdoc: {
      dist: {
        src: ['src/**/*.js', 'README.md'],
        options: {
          destination: 'doc',
        },
      },
    },
    eslint: {
      options: {
        fix: grunt.option('fix'), // this will get params from the flags
      },
      target: ['src/scripts/*.js', 'Gruntfile.js'],
    },
  });

  grunt.loadNpmTasks('grunt-jsdoc');
  grunt.loadNpmTasks('grunt-eslint');

  grunt.registerTask('default', ['all']);
  grunt.registerTask('all', ['lint', 'doc']);

  grunt.registerTask('lint', ['eslint']);
  grunt.registerTask('doc', ['jsdoc']);
};
